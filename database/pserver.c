#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>

#define PORT 8085
#define MAX_CLIENTS 512

pthread_t tid[MAX_CLIENTS];

const char *projectPath = "home/fadhil/fp-sisop-f07-2022/database";
const char *userTable = "home/fadhil/fp-sisop-f07-2022/database/databases/administrator/user.txt";
const char *permissionTable = "home/fadhil/fp-sisop-f07-2022/database/databases/administrator/permission.txt";

void makeDaemon();
bool isSocketClosed(int valRead, int *newSocket);
bool isUserExist(char *database);
bool isUserExist(char *user);
void generateCommand(int *newSocket, char *useDatabase);
void appendLog(char *loginUser, char *command);
char* trim(char *text);
void file(char path[512],char toFile[512]);
int createUser(const char *buffer, char *userType);
int use(const char *buffer, char *userType, char *loginUser, char *useDatabase);
int createDatabase(const char *buffer, char *userType, char *loginUser);
int grantPermission(const char *buffer, char *userType);
int createTable(const char *buffer,char *useDatabase);
void *deleteDir(void *arg);
int dropDatabase(const char *buffer, char *userType, char *loginUser, char *useDatabase);
int validate(const char *buffer);
int insertTable(const char *buffer, char *useDatabase);
int dropTable(const char *buffer, char *useDatabase);
int dropColumn(const char *buffer, char *useDatabase);
int login(char* userType, char *loginUser);
int deleteFrom(const char *buffer,char *useDatabase);
int update(const char *buffer, char *useDatabase);
int selectTable(const char *buffer, char *useDatabase, int *newSocket);
void *play(void *arg);

int main(int argc,const char *argv[]) {
    char dbPath[512] = {0}, adminPath[512] = {0};
    sprintf(dbPath, "%s/databases", projectPath);
    sprintf(adminPath, "%s/administrator", dbPath);

    mkdir(dbPath, 0777);
    mkdir(adminPath, 0777);
    int serverFd, newSocket[MAX_CLIENTS];
    struct sockaddr_in address;
    int opt = 1;
    int addrLen = sizeof(address);
    
    if ((serverFd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket failed!");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(serverFd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);
      
    if (bind(serverFd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Socket bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(serverFd, 3) < 0) {
        perror("Socket listened");
        exit(EXIT_FAILURE);
    }
    makeDaemon();
    int ctr = 0;
    while(1) {
        if ((newSocket[ctr] = accept(serverFd, (struct sockaddr *)&address, (socklen_t*)&addrLen))<0) {
            perror("Accept\n");
            exit(EXIT_FAILURE);
        }
        pthread_create(&(tid[ctr]), NULL, play, &newSocket[ctr]);
        ctr++;
        printf("Client %d terhubung\n", ctr);
    }
    return 0;
}

void makeDaemon() {
    pid_t child, childSID;

    child = fork();

    if(child < 0) exit(EXIT_FAILURE);
    else if(child > 0) exit(EXIT_SUCCESS);

    umask(0);
    childSID = setsid();

    if(childSID < 0) exit(EXIT_FAILURE);

    if((chdir("/")) < 0) exit(EXIT_FAILURE);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

bool isSocketClosed(int valRead, int *newSocket) {
    if(valRead == 0){
        close(*newSocket);
        return 1;
    }
    return 0;
}

bool isDatabaseExist(char *database){
    FILE *fileIn;
    fileIn = fopen(permissionTable,"r");

    char tmpDatabase[512],tmpUser[512];

    if(fileIn) {
        while(fscanf(fileIn, "%s %s", tmpDatabase, tmpUser) != EOF){
            if(!strcmp(tmpDatabase, database)){
                return 1;
            }
        }
    }
    return 0;
}

bool isUserExist(char *user){
    FILE *fileIn;
    fileIn = fopen(userTable, "r");

    char tempUser[512], tmpPass[512];

    if(fileIn){
        while(fscanf(fileIn, "%s %s", tempUser, tmpPass) != EOF){
            if(!strcmp(tempUser, user)){
                return 1;
            }
        }
    }
    return 0;
}

void generateCommand(int *newSocket, char *useDatabase){
    DIR *dp;
    struct dirent *ep;
    char filePath[512] = {0};
    sprintf(filePath, "%s/databases/%s", projectPath, useDatabase);

    dp = opendir(filePath);

    if (dp != NULL) {
        while ((ep = readdir (dp))) {
          if(!strncmp(ep->d_name, "struktur_", 9)){
              char tableName[512] = {0};
              char temp[512] = {0};
              strcpy(temp,ep->d_name);
              char *token = strtok(temp, "_");
              token = strtok(NULL, "_");
              strcpy(tableName, token);

              FILE *fileIn;
              char path[512] = {0};
              sprintf(path,"%s/databases/%s/%s", projectPath, useDatabase, ep->d_name);

              char column[100][512] = {0};
              int totColumn = 0;
              fileIn = fopen(path,"r");
              if(fileIn){
                char temp2[512];
                while((fscanf(fileIn,"%[^\n]%*c", temp2)) != EOF){
                    strcpy(column[totColumn++],temp2);
                }
                fclose(fileIn);

                int valRead;
                char command[512] = {0}, buffer[1024] = {0}, table[512] = {0};
                
                strcpy(table, tableName);
                char *tokenTable = strtok(table,".");
                sprintf(command, "DROP TABLE %s;", tokenTable);
                send(*newSocket, command, strlen(command), 0);
                valRead = recv(*newSocket, buffer, 1024, 0);
            
                bzero(command, sizeof(command));
                sprintf(command, "CREATE TABLE %s (", tokenTable);
                for(int i = 0; i < totColumn; i++){
                    strcat(command, column[i]);
                    strcat(command,",");
                }
                command[strlen(command)-1] = ')';
                strcat(command,";");

                send(*newSocket, command, strlen(command), 0);
                valRead = recv(*newSocket, buffer, 1024, 0);

                bzero(path, sizeof(path));
                sprintf(path,"%s/databases/%s/%s", projectPath, useDatabase, tableName);

                fileIn = fopen(path, "r");
                if(fileIn){
                    while((fscanf(fileIn,"%[^\n]%*c",temp2)) != EOF){
                        bzero(command, sizeof(command));
                        sprintf(command,"INSERT INTO %s VALUES (%s);", tokenTable, temp2);
                        send(*newSocket, command, strlen(command), 0);
                        valRead = recv(*newSocket, buffer, 1024, 0);
                    }
                    fclose(fileIn);
                }
            }
        }
    }
    char command[512] = {0};
    bzero(command,sizeof(command));
    strcpy(command,"SUCCESS");
    send(*newSocket, command, strlen(command), 0);
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

void appendLog(char *loginUser, char *command){
    FILE *fileout;
    char filePath[512]={0};
    sprintf(filePath,"%s/dblog.log", projectPath);
    fileout = fopen(filePath,"a");

    char timeNow[100] = {0};
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	strftime(timeNow,100,"%Y-%m-%d %H:%M:%S", tm);
    fprintf(fileout, "%s:%s:%s\n", timeNow, loginUser, command);
	fclose(fileout);
}

char* trim(char *text){
    int index = 0;
    while(text[index] == ' ' || text[index] == '\t'){
        index++;
    }
    char *temp = strchr(text, text[index]);
    return temp;
}

void file(char path[512], char toFile[512]){
    FILE* ptr = fopen(path,"a");
    fprintf(ptr, "%s\n", toFile);
    fclose(ptr);
}

int createUser(const char *buffer, char *userType){
    char tempBuffer[1024] = {0};
    strcpy(tempBuffer, buffer);
   
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer, " ");
    char input[10][512];
    int i = 0;

    if(strcmp(userType, "root")){
        return 0;
    }

    while (token != NULL) {
        strcpy(input[i++], token);
        token = strtok(NULL, " ");
    }

    if(strcmp("IDENTIFIED", input[3]) || strcmp("BY",input[4]) || buffer[strlen(buffer)-1] != ';'){
        return -1;
    }

    if(i < 6){
        return -1;
    }

    if(!isUserExist(input[2]) && strcmp(input[2], "root") != 0){
        char tmp[512];
        strcpy(tmp,input[2]);
        strcat(tmp," ");
        strcat(tmp,input[5]);
        file(userTable,tmp);
        return 1;
    }
    else {
        return -2;
    }
}

int use(const char *buffer, char *userType, char *loginUser, char *useDatabase){
    char tempBuffer[1024] = {0}, database[100] = {0};
    strcpy(tempBuffer, buffer);
    
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer, " ");
    token = strtok(NULL, " ");
    
    if(token != NULL){
        strcpy(database, token);
    }
    else{
        return -1;
    }

    FILE *fileIn;
    fileIn = fopen(permissionTable, "r");

    char tmpDatabase[512], tmpUser[512];
    bool exist = false;
    if(fileIn){
        while(fscanf(fileIn, "%s %s", tmpDatabase, tmpUser) != EOF){
            if(!strcmp(tmpDatabase, database)){
                exist = true;
                if(!strcmp(tmpUser, loginUser) || !strncmp(userType,"root",4)){
                    fclose(fileIn);
                    strcpy(useDatabase, database);
                    return 1;
                }
            }
        }
    }
    if(!exist){
        return -2;
    }
    return 0;
}

int createDatabase(const char *buffer, char *userType, char *loginUser){
    char tempBuffer[1024] = {0}, database[100] = {0};
    strcpy(tempBuffer, buffer);
    
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer, " ");
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");

    if(token != NULL){
        strcpy(database,token);
    }
    else{
        return -1;
    }

    char filePath[512] = {0};
    sprintf(filePath, "%s/databases/%s", projectPath, database);
    
    if(mkdir(filePath,0777) == 0){
        char record[512] = {0};
        sprintf(record, "%s %s", database, loginUser);
        file(permissionTable, record);      
        return 1;
    }
    else{
        return 0;
    }
}

int grantPermission(const char *buffer, char *userType) {
    char tempBuffer[1024] = {0};
    strcpy(tempBuffer, buffer);
   
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer, " ");
    char input[10][512];
    int i = 0;

    if(strcmp(userType, "root")) {
        return 0;
    }

    while (token != NULL) {
        strcpy(input[i++], token);
        token = strtok(NULL, " ");
    }

    if(strcmp("INTO",input[3]) || buffer[strlen(buffer)-1] != ';') {
        return -1;
    }

    char tmp[512] = {0};
    if(i < 5) {
        return -1;
    }
    if(isDatabaseExist(input[2])) {
        if(isUserExist(input[4])) {
            sprintf(tmp, "%s %s", input[2], input[4]);
            
            file(permissionTable,tmp);
            return 1;
        }
        else {
            return -3;
        }
    }
    else {
        return -2;
    }
}

int createTable(const char *buffer, char *useDatabase){
    if(strlen(useDatabase) == 0){
        return -2;
    }
    char tempBuffer[1024] = {0}, table[100] = {0};
    strcpy(tempBuffer,buffer);
    
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");

    if(token != NULL) {
        strcpy(table, token);
    }
    else {
        return -1;
    }

    char *temp;
    temp = strchr(buffer, '(');
    if(!temp) {
        return -1;
    }
    else {
        temp = temp + 1;
    }

    char column[512] = {0};
    strcpy(column, temp);

    char *token1 = strtok(column, ";");
    token1 = strtok(column, ")");

    char splitColumn[100][100] = {0};
    int ind = 0;
    token1 = strtok(column, ",");
    while(token1 != NULL){
        strcpy(splitColumn[ind++], trim(token1));
        token1 = strtok(NULL, ",");
    }

    for(int i = 0; i < ind; i++){
        char temp2[512] = {0},nama_kolom[100] = {0}, tipe_kolom[20] = {0};
        strcpy(temp2,splitColumn[i]);
        char *token2 = strtok(temp2," ");
        strcpy(nama_kolom,token2);
        token2 = strtok(NULL," ");
        if(token2){
            strcpy(tipe_kolom,token2);
            if(strcmp(tipe_kolom,"int") && strcmp(tipe_kolom,"string")){
                return -4;
            }
        }else{
            return -3;
        }
    }

    char filePath[512] = {0};
    sprintf(filePath,"%s/databases/%s/%s.txt",projectPath,useDatabase,table);
    FILE *open;
    if(!(open = fopen(filePath,"r"))){
        open = fopen(filePath,"w");
        char struktur_table[512] = {0};
        sprintf(struktur_table,"%s/databases/%s/struktur_%s.txt",projectPath,useDatabase,table);

        for(int i = 0; i < ind; i++){
            file(struktur_table,splitColumn[i]);
        }
        return 1;
    }else{
        return 0;
    }
}

void *deleteDir(void *arg){
    pid_t child;
    child = fork();
    if(child == 0){
        char *filePath = (char *) arg;
        char *argv[] = {"rm","-rf",filePath, NULL};
        execv("/bin/rm",argv);
    }
}

int dropDatabase(const char *buffer, char *userType, char *loginUser, char *useDatabase){

    char tempBuffer[1024] = {0}, database[100] = {0};
    strcpy(tempBuffer,buffer);
    
    
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");
    
    if(token != NULL){
        strcpy(database,token);
    }else{
        return -1;
    }

    FILE *fileIn, *fileout;
    fileIn = fopen(permissionTable,"r");


    char tmpDatabase[512],tmpUser[512];
    bool exist = false,able =false;
    if(fileIn){
        while(fscanf(fileIn,"%s %s",tmpDatabase,tmpUser) != EOF){
            if(!strcmp(tmpDatabase,database)){
                exist = true;
                if(!strcmp(tmpUser,loginUser)){
                    fclose(fileIn);
                    able = true;
                    break;
                }
            }
        }

        if(able || (exist && !strcmp(loginUser,"root"))){              
            char filePath[512] = {0};
            sprintf(filePath,"%s/databases/%s",projectPath,database);
            pthread_t thread1;
            int iret1 = pthread_create(&thread1,NULL,deleteDir,filePath);;
            pthread_join(thread1,NULL);
            
            fileIn = fopen(permissionTable,"r");
            char temp_permission[512] = {0};
            sprintf(temp_permission,"%s/databases/administrator/temp.txt",projectPath);
            fileout = fopen(temp_permission,"w");
            while(fscanf(fileIn,"%s %s",tmpDatabase,tmpUser) != EOF){
                if(strcmp(tmpDatabase,database)){
                    char record[512] = {0};
                    fprintf(fileout, "%s %s\n", tmpDatabase, tmpUser);
                }
            }
            fclose(fileout);
            remove(permissionTable);
            rename(temp_permission,permissionTable);

            if(!strcmp(useDatabase,database)){
                bzero(useDatabase,sizeof(useDatabase));
            }
            return 1;
        }        
    }
    if(!exist){
        return -2;
    }
    return 0;
}

int validate(const char *buffer){
    char tmp[512];
    strcpy(tmp,buffer);
    char g = '\'';

    if(tmp[0]== g && tmp[strlen(tmp)-1] == g){
        return 1;
    }
    for(int i = 0; i<strlen(tmp) ;i++){
        if(tmp[i] < '0' || tmp[i] > '9'){
            return 0;
        }
    }
    return 2;
}

int insertTable(const char *buffer, char *useDatabase){

    if(!strlen(useDatabase)){
        return -1;
    }    

    char tmp[512];
    strcpy(tmp,buffer);

    char *token = strtok(tmp,"(");
    token = strtok(NULL,"(");
    token = strtok(token,")");
    token = strtok(token,",");

    char data[100][512];

    int i = 0;

    while(token!=NULL){
        strcpy(data[i],trim(token));
        i++;
        token = strtok(NULL,",");
    }

    FILE *fileIn,*fileout;

    strcpy(tmp,buffer);
    token = strtok(tmp," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    char open[512] = {0},append[512] = {0};
    sprintf(open,"%s/databases/%s/struktur_%s.txt",projectPath,useDatabase,token);
    fileIn = fopen(open,"r");

   
    char data_type[100][512];
    char tmp_type[512],ret[512];
    int k = 0;

    if(fileIn){
        while(fscanf(fileIn,"%s %s",ret,tmp_type) != EOF){
            strcpy(data_type[k],tmp_type);
            k++;
        }
    }
    else{
        return -2;
    }

    sprintf(append,"%s/databases/%s/%s.txt",projectPath,useDatabase,token);
    fileout = fopen(append,"a");

    if(k != i){
        fclose(fileIn);
        fclose(fileout);
        return -3;
    }

    for(int j=0;j<i;j++){
        int val = validate(data[j]);
        if(val == 1 && !strcmp(data_type[j],"string")) ;
        else if (val == 2 && !strcmp(data_type[j],"int")) ;
        else {
            fclose(fileIn);
            fclose(fileout);
            return -4;
        }  
    }
    for(int j=0;j<i-1;j++){
        fprintf(fileout,"%s,",data[j]);
    }
    fprintf(fileout,"%s\n",data[i-1]);
    fclose(fileIn);
    fclose(fileout);

    return 1;
}

int dropTable(const char *buffer, char *useDatabase){

    if(!strlen(useDatabase)){
        return -2;
    }

    FILE *fileIn;

    char tmp[512];
    char *token;
    strcpy(tmp,buffer);
    token = strtok(tmp,";");
    token = strtok(token," ");
    token = strtok(NULL," ");
    token = strtok(NULL," ");

    char open[512] = {0},append[512]={0};
    sprintf(open,"%s/databases/%s/struktur_%s.txt",projectPath,useDatabase,token);
    fileIn = fopen(open,"r");

    sprintf(append,"%s/databases/%s/%s.txt",projectPath,useDatabase,token);

    if(fileIn){
        fclose(fileIn);
        remove(open);
        remove(append);
        return 1;
    }
    else{
        return -1;
    }
}

int dropColumn(const char *buffer, char *useDatabase){

    if(!strlen(useDatabase)){
        return -2;
    }

    char input[10][512];
    char tmp[10000];
    strcpy(tmp,buffer);
    int k = 0;
    char *token;
    token = strtok(tmp,";");
    token = strtok(token," ");
    while (token != NULL) {
        strcpy(input[k++],token);
        token = strtok(NULL, " ");
    }

    if(k != 5){
        return -3;
    }

    FILE *strukturIn, *strukturout, *tablein, *tableout;

    char open[512]={0},append[512]={0},a[512]={0},b[512]={0};
    sprintf(open,"%s/databases/%s/struktur_%s",projectPath,useDatabase,input[4]);
    strcpy(a,open);
    strcat(a,"2.txt");
    strcat(open,".txt");

    strukturIn = fopen(open,"r");
   
    int i, j;
    i = j = 0;
    char data_type[512], name[512];

    if(strukturIn){
        strukturout = fopen(a,"w");
        while(fscanf(strukturIn,"%s %s",name,data_type) != EOF){
            if(strcmp(input[2],name)){
                fprintf(strukturout,"%s %s\n",name,data_type);
            }
            else i = j;
            j++;
        }
        fclose(strukturIn);
        fclose(strukturout);
    }
    else{
        return -1;
    }

    sprintf(append,"%s/databases/%s/%s",projectPath,useDatabase,input[4]);
    strcpy(b,append);
    strcat(b,"2.txt");
    strcat(append,".txt");

    tablein = fopen(append,"r");
    tableout = fopen(b,"w");

    char getVal[512];
    while(fgets(getVal,512,tablein)){
        token = strtok(getVal,",");
        int j = 0;
        char newVal[512];
        strcpy(newVal,"");
        while(token!=NULL){
            if(j!=i){
                strcat(newVal,token);
                strcat(newVal,",");
            }
            token = strtok(NULL,",");
            j++;
        }
        newVal[strlen(newVal)-1] = 0;
        fprintf(tableout,"%s",newVal);
        if(newVal[strlen(newVal)-1] != '\n'){
            fprintf(tableout,"\n");
        }
    }
    fclose(tablein);
    fclose(tableout);
    remove(open);
    rename(a,open);
    remove(append);
    rename(b,append);
    return 1;
}

int login(char* userType, char *loginUser){
   
    FILE *fileIn;
    fileIn = fopen(userTable,"r");
    printf("%s\n",userType);
    char user[512],pass[512];

    char* token = strtok(userType, " ");
    strcpy(user,token);
    token = strtok(NULL, " ");
    strcpy(pass,token);

    char tmpUser[512],tmpPass[512];
    if(fileIn){
        while(fscanf(fileIn,"%s %s",tmpUser,tmpPass) != EOF){
            if(!strcmp(tmpUser,user) && !strcmp(tmpPass,pass)){
                fclose(fileIn);
                strcpy(loginUser,user);
                return 1;
            }
        }
        fclose(fileIn);
    }

    return 0;
    
}

int deleteFrom(const char *buffer,char *useDatabase){

    if(!strlen(useDatabase)){
        return -1;
    }

    char tempBuffer[1024] = {0};
    strcpy(tempBuffer,buffer);
   
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    char input[10][512];
    int i=0;

    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    if(i == 3){
        char path[10000];
        sprintf(path,"%s/databases/%s/%s.txt",projectPath,useDatabase,input[2]);
        FILE *fileIn,*fileout;
        fileIn = fopen(path,"r");
        if(fileIn){
            fclose(fileIn);
            fileout = fopen(path,"w");
            fprintf(fileout,"");
            fclose(fileout);
        }
        else{
            return -3;
        }
        return 1;
    }
    else if(i == 5){
        if(strcmp(input[3],"WHERE")){
            return -2;
        }

        char tableName[512], cmp[512];

        token = strtok(input[4],"=");
        strcpy(tableName,token);
        token = strtok(NULL,"=");
        strcpy(cmp,token);

        char path[10000],struk[10000],nPath[10000];

        sprintf(struk,"%s/databases/%s/struktur_%s.txt",projectPath,useDatabase,input[2]);
        FILE *strukturIn;
        strukturIn = fopen(struk,"r");

        int i = -1;
        int k = 0;
        char ret[512],table[512];
        if(strukturIn){
            while(fscanf(strukturIn,"%s %s",table,ret) != EOF){
                if(!strcmp(tableName,table)){
                    i = k;
                }
                k++;
            }
            if(i == -1){
                fclose(strukturIn);
                return -4;
            }
            fclose(strukturIn);
        }
        else{
            return -3;
        }

        sprintf(path,"%s/databases/%s/%s.txt",projectPath,useDatabase,input[2]);
        sprintf(nPath,"%s/databases/%s/%s2.txt",projectPath,useDatabase,input[2]);
        FILE *fileIn,*fileout;
        fileIn = fopen(path,"r");
        fileout = fopen(nPath,"w");
        char getVal[512];
        if(fileIn){
            while(fgets(getVal,512,fileIn)){
                token = strtok(getVal,",");
                int j = 0;
                char newVal[512];
                strcpy(newVal,"");
                int flag = 0;
                while(token!=NULL){
                    strcat(newVal,token);
                    strcat(newVal,",");
                    if(j==i){
                        if(!strcmp(cmp,token)){
                            flag = 1;
                        }
                    }
                    token = strtok(NULL,",");
                    j++;
                }
                if(!flag){
                    newVal[strlen(newVal)-1] = 0;
                    fprintf(fileout,"%s",newVal);
                    if(newVal[strlen(newVal)-1] != '\n'){
                        fprintf(fileout,"\n");
                    }
                }
            }
            fclose(fileIn);
            fclose(fileout);
            remove(path);
            rename(nPath,path);
            return 1;
        }
        else{
            return -3;
        }
    }
    else{
        return -2;
    }
}

int update(const char *buffer,char *useDatabase){
    
    if(!strlen(useDatabase)){
        return -1;
    }

    char tempBuffer[1024] = {0};
    strcpy(tempBuffer, buffer);
   
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    char input[10][512];
    int i=0;

    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    if(strcmp(input[2],"SET")){
        return -2;
    }

    char tableName[512], cmp[512];

    token = strtok(input[3],"=");
    strcpy(tableName,token);
    token = strtok(NULL,"=");
    strcpy(cmp,token);

    char path[10000],struk[10000],nPath[10000];

    sprintf(struk,"%s/databases/%s/struktur_%s.txt", projectPath, useDatabase, input[1]);
    FILE *strukturIn;
    strukturIn = fopen(struk,"r");

    i = -1;
    int k = 0;
    char ret[512],table[512];
    if(strukturIn){
        while(fscanf(strukturIn,"%s %s",table,ret) != EOF){
            if(!strcmp(tableName,table)){
                i = k;
            }
            k++;
        }
        if(i == -1){
            fclose(strukturIn);
            return -4;
        }
        fclose(strukturIn);
    }
    else{
        return -3;
    }

    int w = -1;
    char banding[512],temp[512],cmpWhere[512];

    if(strlen(input[4])){
        token = strtok(input[5],"=");
        strcpy(banding,token);
        token = strtok(NULL,"=");
        strcpy(cmpWhere,token);
        k=0;
        if(!strcmp(input[4],"WHERE")){
            strukturIn = fopen(struk,"r");
            while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                if(!strcmp(banding,temp)){
                    w = k;
                }
                k++;
            }
            if(w == -1){
                fclose(strukturIn);
                return -4;
            }
            fclose(strukturIn);
        }
    }

    sprintf(path,"%s/databases/%s/%s.txt",projectPath, useDatabase,input[1]);
    sprintf(nPath,"%s/databases/%s/%s2.txt",projectPath,useDatabase,input[1]);
    FILE *fileIn,*fileout;
    fileIn = fopen(path,"r");
    fileout = fopen(nPath,"w");
    char getVal[512], oldVal[512];
    if(fileIn){
        while(fgets(getVal,512,fileIn)){
            strcpy(oldVal,getVal);
            token = strtok(getVal,",");
            int j = 0;
            char newVal[512];
            strcpy(newVal,"");
            int flag = 0;
            while(token!=NULL){
                if(j == w){
                    if(!strcmp(cmpWhere,token)){
                        flag = 1;
                    }
                }
                if(j==i){
                    strcat(newVal,cmp);
                    strcat(newVal,",");
                }
                else{
                    strcat(newVal,token);
                    strcat(newVal,",");
                }
                token = strtok(NULL,",");
                j++;
            }
            if(flag == 1 || w == -1){
                newVal[strlen(newVal)-1] = 0;
                fprintf(fileout,"%s",newVal);
                if(newVal[strlen(newVal)-1] != '\n'){
                    fprintf(fileout,"\n");
                }
            }
            else{
                oldVal[strlen(newVal)-1] = 0;
                fprintf(fileout,"%s",oldVal);
                if(oldVal[strlen(oldVal)-1] != '\n'){
                    fprintf(fileout,"\n");
                }
            }
        }
        fclose(fileIn);
        fclose(fileout);
        remove(path);
        rename(nPath,path);
        return 1;
    }
}

int selectTable(const char *buffer, char *useDatabase, int *newSocket){
    if(!strlen(useDatabase)){
        return -1;
    }

    char tempBuffer[1024] = {0};
    strcpy(tempBuffer,buffer);
   
    char* token = strtok(tempBuffer, ";");
    token = strtok(tempBuffer," ");
    char input[10][512];
    int i=0;

    while (token != NULL) {
        strcpy(input[i++],token);
        token = strtok(NULL, " ");
    }

    int all,w = -1,k,cases,urut[20];
    char banding[512],temp[512],cmpWhere[512],ret[512];
    char path[10000],struk[10000],nPath[10000],cmp[20][512];

    FILE *strukturIn;

    if(!strcmp(input[1],"*")){
        sprintf(struk,"%s/databases/%s/struktur_%s.txt", projectPath, useDatabase, input[3]);

        strukturIn = fopen(struk,"r");

        if(!strukturIn){
            return -3;
        }

        fclose(strukturIn);

        all = -1;
        cases = 2;
        if(strcmp(input[2],"FROM")){
            return -2;
        }
        if(i >= 5 && !strcmp(input[4], "WHERE")){
            token = strtok(input[5], "=");
            strcpy(banding, token);
            token = strtok(NULL, "=");
            strcpy(cmpWhere, token);

            k=0;

            if(!strcmp(input[4],"WHERE")){
                strukturIn = fopen(struk,"r");
                while(fscanf(strukturIn,"%s %s",temp, ret) != EOF){
                    if(!strcmp(banding,temp)){
                        w = k;
                    }
                    k++;
                }
                if(w == -1){
                    fclose(strukturIn);
                    return -4;
                }
                fclose(strukturIn);
            }
        }
    }

    else{
        cases = 2;
        while(cases <= i && strcmp(input[cases++],"FROM")){
        }

        if(cases > i){
            return -2;
        }
        sprintf(struk,"%s/databases/%s/struktur_%s.txt",projectPath,useDatabase,input[cases]);
        strukturIn = fopen(struk,"r");

        if(!strukturIn){
            printf("ka %d %s\n",cases,struk);
            return -3;
        }

        fclose(strukturIn);

        all = 0;
        cases = 1;
        
        for(int oo = 0; oo<20;oo++){
            urut[oo] = -1;
        }

        char temp2[512];
        while(cases < i && strcmp(input[cases],"FROM")){
            k=0;
            strukturIn = fopen(struk,"r");
            while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                strcpy(temp2,temp);
                strcat(temp2,",");
                if(!strcmp(input[cases],temp) || !strcmp(temp2,input[cases])){
                    strcpy(cmp[all],input[cases]);
                    urut[all] = k;
                    all++;
                }
                k++;
            }
            if(all == 0 || urut[all-1] == -1){
                fclose(strukturIn);
                return -4;
            }
            fclose(strukturIn);
            cases++;
        }
        if(cases == i){
            return -3;
        }
        if(i >= cases+2 && !strcmp(input[cases+2],"WHERE")){
            token = strtok(input[cases+3],"=");
            strcpy(banding,token);
            token = strtok(NULL,"=");
            strcpy(cmpWhere,token);

            k=0;

            if(!strcmp(input[cases+2],"WHERE")){
                strukturIn = fopen(struk,"r");
                while(fscanf(strukturIn,"%s %s",temp,ret) != EOF){
                    if(!strcmp(banding,temp)){
                        w = k;
                    }
                    k++;
                }
                if(w == -1){
                    fclose(strukturIn);
                    return -4;
                }
                fclose(strukturIn);
            }
        }
    }

    sprintf(path,"%s/databases/%s/%s.txt",projectPath,useDatabase,input[cases+1]);
    sprintf(nPath,"%s/databases/%s/%s2.txt",projectPath,useDatabase,input[cases+1]);
    FILE *fileIn,*fileout;
    fileIn = fopen(path,"r");
    fileout = fopen(nPath,"w");
    char getVal[512], oldVal[512],newVal[512];

    if(fileIn){
        char init[100] = {0},confirm[1024]={0};
        strcpy(init,"BEGIN");
        send(*newSocket, init, strlen(init), 0);
        int valRead = recv(*newSocket, confirm, 1024, 0);
        while(fgets(getVal,512,fileIn)){
            if(all == -1){
                if(w == -1){
                    char text[512] = {0},buffer2[512] = {0};
                    strcpy(text, getVal);
                    send(*newSocket, text, strlen(text), 0);
                    valRead = recv(*newSocket, buffer2, 1024, 0);
                }
                else{
                    char hade[512];
                    strcpy(hade,getVal);
                    token = strtok(getVal, ",");
                    int j = 0;
                    strcpy(newVal,"");
                    int flag = 0;
                    while(token!=NULL){
                        if(j == w){
                            if(!strcmp(cmpWhere, token)){
                                char text[512] = {0}, buffer2[512] = {0};
                                strcpy(text,hade);
                                send(*newSocket, text, strlen(text), 0);
                                valRead = recv(*newSocket, buffer2, 1024, 0);
                                break;
                            }
                        }
                        token = strtok(NULL,",");
                        j++;
                    }
                }
            }
            else{
                char jadi[512], hade[512];
                strcpy(jadi, "");
                for(int oo = 0; oo < all; oo++){
                    strcpy(hade, getVal);
                    token = strtok(hade, ",");
                    int j = 0;
                    strcpy(newVal,"");
                    int flag = 0;
                    while(token != NULL){
                        if(j == urut[oo]){
                            strcat(jadi,token);
                            strcat(jadi,",");
                            break;
                        }
                        token = strtok(NULL,",");
                        j++;
                    }
                }
                char text[512] = {0}, buffer2[512] = {0};
                jadi[strlen(jadi)-1]='\0';
                strcpy(text, jadi);
                send(*newSocket, text, strlen(text), 0);               
                valRead = recv(*newSocket, buffer2, 1024, 0);
            }
        }
        char text[512] = {0}, buffer2[1024] = {0};
        strcpy(text, "SUCCESS");
        send(*newSocket, text, strlen(text), 0);
        valRead = recv(*newSocket, buffer2, 1024, 0);
    }
    else{
        return -2;
    }
}

void *play(void *arg){
    bool modeDump = false;
    int *newSocket = (int *) arg;
    int valRead;
    char userType[1024] = {0}, tmptipe[1024] = {0};
    valRead = recv(*newSocket, userType, 1024, 0);
    char loginUser[100] = {0}, useDatabase[100]={0};

    strcpy(tmptipe,userType);

    if(!strncmp(userType, "root", 4)){
        strcpy(loginUser, "root");
        char statusLogin[512] = {0};
        strcpy(statusLogin, "authentication success");
        send(*newSocket, statusLogin, strlen(statusLogin), 0);

        char *ret = strstr(tmptipe, "dump");
        if(ret){
            modeDump = true;
        }
    }
    else{
        int masuk = login(userType, loginUser);
        char statusLogin[512] = {0};
        
        if(masuk == 0){
            strcpy(statusLogin,"authentication failed");
            send(*newSocket, statusLogin, strlen(statusLogin), 0);
            printf("login gagal\n");
            close(*newSocket);
            return;
        }
        strcpy(statusLogin,"authentication success");
        send(*newSocket, statusLogin, strlen(statusLogin), 0);

        printf("berhasil login %s\n",loginUser);

        char *ret = strstr(tmptipe,"dump");
        if(ret){
            modeDump = true;
        }
    }

    if(modeDump){
        char buffer[1024] = {0}, message[1024] = {0};
        valRead = recv(*newSocket, buffer, 1024, 0);
        if(!strncmp(buffer, "USE", 3)){
            appendLog(loginUser, buffer);
            int status = use(buffer, userType, loginUser, useDatabase);
            if(status == -2){
                strcpy(message,"unknown database");
                close(*newSocket);
            }
            else if(status == -1){
                strcpy(message,"syntax error");
                close(*newSocket);
            }
            else if(status == 1){
                sprintf(message,"database changed to %s", useDatabase);
            }
            else if(status == 0){
                strcpy(message, "permission denied");
                close(*newSocket);
            }
            send(*newSocket, message, strlen(message), 0);
            valRead = recv(*newSocket, buffer, 1024, 0);

            generateCommand(newSocket, useDatabase);
        }

        close(*newSocket);
        return 0;
    }
    while(1){
        char buffer[1024] = {0};
        char message[1024] = {0};
        char *hello = "Hello from server";

        valRead = recv(*newSocket, buffer, 1024, 0);
        printf("%s\n", buffer);

        if(isSocketClosed(valRead,newSocket)){
            printf("Koneksi terputus\n");
            break;
        }

        if(!strncmp(buffer, "CREATE USER", 11)){
            appendLog(loginUser, buffer);
            int status = createUser(buffer,userType);
            if(status == -2){
                strcpy(message,"user already exist");
            }
            else if(status == -1){
                strcpy(message, "syntax error");
            }
            else if(status == 0){
                strcpy(message, "permission denied");
            }
            else if(status == 1){
                strcpy(message, "create user success");
            }
        }
        else if(!strncmp(buffer, "USE", 3)){
            appendLog(loginUser, buffer);
            int status = use(buffer, userType, loginUser, useDatabase);
            if(status == -2){
                strcpy(message,"unknown database");
            }
            else if(status == -1){
                strcpy(message, "syntax error");
            }
            else if(status == 1){
                sprintf(message, "database changed to %s", useDatabase);
            }
            else if(status == 0){
                strcpy(message, "permission denied");
            }
        }
        else if(!strncmp(buffer, "CREATE DATABASE", 15)){
            appendLog(loginUser, buffer);
            int status = createDatabase(buffer, userType, loginUser);
            if(status == -1){
                strcpy(message, "syntax error");
            }
            else if(status == 1){
                strcpy(message, "create success");
            }
            else if(status == 0){
                strcpy(message, "permission denied");
            }
        }
        else if(!strncmp(buffer, "GRANT PERMISSION", 16)){
            appendLog(loginUser, buffer);
            int status = grantPermission(buffer,userType);
            if(status == -3){
                strcpy(message,"unknown user");
            }
            else if(status == -2){
                strcpy(message,"database not exist");
            }
            else if(status == -1){
                strcpy(message,"syntax error");
            }
            else if(status == 1){
                strcpy(message,"grant permission success");
            }
            else if(status == 0){
                strcpy(message,"grant permission denied");
            }
        }
        else if(!strncmp(buffer,"CREATE TABLE", 12)){
            appendLog(loginUser, buffer);
            int status = createTable(buffer, useDatabase);
            if(status == -4){
                strcpy(message,"invalid column type");
            }
            else if(status == -3){
                strcpy(message,"missing column type");
            }
            else if(status == -2){
                strcpy(message,"no database used");
            }
            else if(status == -1){
                strcpy(message,"syntax error");
            }
            else if(status == 1){
                strcpy(message,"create success");
            }
            else if(status == 0){
                strcpy(message,"table already exist");
            }
        }
        else if(!strncmp(buffer,"DROP DATABASE", 13)){
            appendLog(loginUser, buffer);
            int status = dropDatabase(buffer, userType, loginUser, useDatabase);
            if(status == -2){
                strcpy(message,"unknown database");
            }
            else if(status == -1){
                strcpy(message,"syntax error");
            }
            else if(status == 1){
                sprintf(message,"database dropped");
            }
            else if(status == 0){
                strcpy(message,"permission denied");
            }
        }
        else if(!strncmp(buffer, "INSERT INTO", 11)){
            appendLog(loginUser, buffer);
            int status = insertTable(buffer, useDatabase);
            switch (status) {
                case 1:
                    strcpy(message,"Insert success");
                    break;
                case -1:
                    strcpy(message,"no database used");
                    break;
                case -2:
                    strcpy(message,"table does not exist");
                    break;
                case -3:
                    strcpy(message,"coloumn count doesnt match");
                    break;
                case -4:
                    strcpy(message,"invalid input");
                    break;
            }
        }else if(!strncmp(buffer, "DROP TABLE", 10)){
            appendLog(loginUser, buffer);
            int status = dropTable(buffer, useDatabase);
            switch (status) {
                case 1:
                    strcpy(message,"drop table success");
                    break;
                case -1:
                    strcpy(message,"table does not exist");
                    break;
                case -2:
                    strcpy(message,"no database used");
                    break;
            }
        }else if(!strncmp(buffer, "DROP COLUMN", 11)){
            appendLog(loginUser, buffer);
            int status = dropColumn(buffer, useDatabase);
            switch (status) {
                case 1:
                    strcpy(message,"drop column success");
                    break;
                case -1:
                    strcpy(message,"table does not exist");
                    break;
                case -2:
                    strcpy(message,"no database used");
                    break;
                case -3:
                    strcpy(message,"invalid syntax");
                    break;
                default:
                    strcpy(message,"error");
                    break;
            }
        }else if(!strncmp(buffer, "DELETE FROM", 11)){
            int status = deleteFrom(buffer, useDatabase);
            switch(status){
                case 1:
                    strcpy(message,"delete success");
                    break;
                case -1:
                    strcpy(message,"no database used");
                    break;
                case -2:
                    strcpy(message,"invalid syntax");
                    break;
                case -3:
                    strcpy(message,"table does not exist");
                    break;
                case -4:
                    strcpy(message,"column does not exist");
                    break;
                default:
                    strcpy(message,"error");
                    break;
            }
        }else if(!strncmp(buffer, "UPDATE", 6)){
            int status = update(buffer, useDatabase);
            switch(status){
                case 1:
                    strcpy(message,"update success");
                    break;
                case -1:
                    strcpy(message,"no database used");
                    break;
                case -2:
                    strcpy(message,"invalid syntax");
                    break;
                case -3:
                    strcpy(message,"table does not exist");
                    break;
                case -4:
                    strcpy(message,"column does not exist");
                    break;
                default:
                    strcpy(message,"error");
                    break;
            }
        }
        else if(!strncmp(buffer, "SELECT", 6)){
            int status = selectTable(buffer, useDatabase, newSocket);
            switch(status){
                case -1:
                    strcpy(message,"no database used");
                    break;
                case -2:
                    strcpy(message,"invalid syntax");
                    break;
                case -3:
                    strcpy(message,"table does not exist");
                    break;
                case -4:
                    strcpy(message,"column does not exist");
                    break;
                default:
                    strcpy(message,"select success");
                    break;
            }
        }
        else{
            appendLog(loginUser, buffer);
            strcpy(message, "Syntax error");
        }

        if(!strcmp(buffer, "STATUS")) {
            sprintf(message, "loginUser:%s useDatabase:%s", loginUser, useDatabase);
        } 

        send(*newSocket, message, strlen(message), 0);
    }
}
