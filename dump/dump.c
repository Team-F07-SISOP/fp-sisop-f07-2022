#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
#define PORT 8085
  
bool checkClose(int valRead, int sock){
    if(valRead == 0){
        close(sock);
        return 1;
    }
    return 0;
}

int main(int argc, char const *argv[]) {    
    if(getuid()){
        if(argc < 6){
            printf("Syntax error\n"); return -1;
        }else{
            if(strcmp(argv[1],"-u") || strcmp(argv[3],"-p"))
                return -1;
        }
    }else{
        if(argc < 2){
            printf("Syntax error\n"); return -1;
        }
    }
    struct sockaddr_in address;
    int sock = 0, valRead;
    struct sockaddr_in servAddr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&servAddr, '0', sizeof(servAddr));
  
    servAddr.sin_family = AF_INET;
    servAddr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &servAddr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&servAddr, sizeof(servAddr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    char type[1024];
    if(getuid()){
        strcpy(type,argv[2]);
        strcat(type," ");
        strcat(type,argv[4]);
        strcat(type," dump ");
        send(sock, type, strlen(type), 0);
    }
    else{
        strcpy(type,"root dump ");
        send(sock, type, strlen(type), 0);
    }
    char statusLogin[1000] = {0};
    int val = recv( sock, statusLogin, 1000,0);
    if(!strcmp(statusLogin,"authentication failed")){
        close(sock);
        return -1;
    }
    char stat_log_in[1000] = {0};
    int val;
    val = recv( sock , stat_log_in, 1000,0);

    if(!strcmp(stat_log_in,"authentication failed")){
        close(sock); return -1;
    }
    if(getuid()){
        char command[1000] = {0}, receive[1000] = {0};
        sprintf(command,"USE %s",argv[5]);
        send(sock, command, strlen(command), 0);
        int val = recv( sock, receive, 1000,0);
        bzero(command,sizeof(command));
        strcpy(command,"ok");
        send(sock, command, strlen(command), 0);
        if(!strncmp(receive,"database changed to",19)){
            do{
                bzero(receive,sizeof(receive));
                int val = recv( sock, receive, 1000,0);
                if(strcmp(receive,"DONE!!!")){
                    printf("%s\n",receive);
                    bzero(command,sizeof(command));
                    strcpy(command,"ok");
                    send(sock, command, strlen(command), 0);
                }
            }
            while(strcmp(receive,"done!"));
            close(sock); return 0;
        }
    }else{
        char command[1000] = {0}, receive[1000] = {0};
        sprintf(command,"USE %s",argv[1]);
        send(sock, command, strlen(command), 0);
        int val = recv( sock, receive, 1000,0);
        bzero(command,sizeof(command));
        strcpy(command,"ok");
        send(sock, command, strlen(command), 0);
        if(!strncmp(receive,"database changed to",19)){
            do{
                bzero(receive,sizeof(receive));
                int val = recv( sock, receive, 1000,0);
                if(strcmp(receive,"DONE!!!")){
                    printf("%s\n",receive);
                    bzero(command,sizeof(command));
                    strcpy(command,"ok");
                    send(sock, command, strlen(command), 0);
                }
            }
            while(strcmp(receive,"done!"));
            close(sock); return 0;
        }
    } return 0;
}
