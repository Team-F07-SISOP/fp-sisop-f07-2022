#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
#define PORT 8085

bool checkConnectionClose(int response, int sock);

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int clientSocket = 0, response;
    struct sockaddr_in serverAddress;

    if ((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serverAddress, '0', sizeof(serverAddress));
  
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(clientSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    bool isDataFromBackup = false;
    int userUID = getuid();

    if(userUID){
        if(argc < 5){
            printf("Not enough argument\n");
            return -1;
        }
        if(strcmp(argv[1],"-u") || strcmp(argv[3],"-p")){
            printf("Syntax error\n");
            return -1;
        }
        if(argc == 7){
            if(!strcmp(argv[5],"-d")){
                isDataFromBackup = true;
            } else{
                printf("Syntax error\n");
                return -1;
            }
        }
    } else{
        if(argc == 3){
            if(!strcmp(argv[1], "-d")){
                isDataFromBackup = true;
            }else{
                printf("Syntax error\n");
                return -1;
            }
        }
    }

    char userType[1024];
    if(userUID){
        strcpy(userType,argv[2]);
        strcat(userType," ");
        strcat(userType,argv[4]);
        send(clientSocket , userType , strlen(userType) , 0 );
    }
    else{
        strcpy(userType,"root");
        send(clientSocket , userType , strlen(userType) , 0 );
    }

    char loginStatus[1000] = {0};
    int val = recv( clientSocket , loginStatus, 1000,0);
    printf("%s\n",loginStatus);
    if(!strcmp(loginStatus,"Authentication failed")){
        close(clientSocket);
        return -1;
    }

    if(userUID){
        if(isDataFromBackup){
            char command[1000] = {0};
            char receive[1000] = {0};
            sprintf(command,"USE %s",argv[6]);
            send(clientSocket , command , strlen(command) , 0 );
            int val = recv( clientSocket , receive, 1000,0);
            if(!strncmp(receive,"database changed to",19)){
                char temp[1000];
                while((fscanf(stdin,"%[^\n]%*c",temp)) != EOF){
                    send(clientSocket , temp , strlen(temp) , 0 );
                    bzero(receive,sizeof(receive));
                    int val = recv( clientSocket , receive, 1000,0);
                    printf("%s\n",receive);
                };
                close(clientSocket);
                return 0;
            }
        }
    }else{
        if(isDataFromBackup){
            char command[1000] = {0}, receive[1000] = {0};
            sprintf(command,"USE %s",argv[2]);
            send(clientSocket , command , strlen(command) , 0 );
            int val = recv( clientSocket , receive, 1000,0);
            if(!strncmp(receive,"database changed to",19)){
                char temp[1000];
                while((fscanf(stdin,"%[^\n]%*c",temp)) != EOF){
                    send(clientSocket , temp , strlen(temp) , 0 );
                    bzero(receive,sizeof(receive));
                    int val = recv( clientSocket , receive, 1000,0);
                    printf("%s\n",receive);
                };
                close(clientSocket);
                return 0;
            }
        }
    }
    while(true){
        char msg[1000] ={0};
        char buffer[1024] = {0};
        gets(msg);
        send(clientSocket, msg, strlen(msg), 0);
        response = recv(clientSocket, buffer, 1024, 0);
        if(checkConnectionClose(response, clientSocket)){
            break;
        }
        if(!strcmp(buffer,"BEGIN")){
            char command[1000] = {0};
            char receive[1000] = {0};
            strcpy(command,"ok");
            send(clientSocket , command , strlen(command) , 0 );
            do {
                bzero(receive,sizeof(receive));
                int val = recv( clientSocket , receive, 1000,0);
                if(strcmp(receive,"SUCCESS")){
                    if(receive[strlen(receive)-1] != '\n'){
                        printf("%s\n",receive);
                    }else{
                        printf("%s",receive);
                    }
                    bzero(command,sizeof(command));
                    strcpy(command,"ok");
                    send(clientSocket , command , strlen(command) , 0 );
                }
            } while(strcmp(receive,"SUCCESS"));
            bzero(command,sizeof(command));
            strcpy(command,"ok");
            send(clientSocket , command , strlen(command) , 0 );
            int val = recv( clientSocket , buffer, 1024,0);
            printf("%s\n",buffer);
        }else{
            printf("%s\n",buffer );
        }
    }
    return 0;
}

bool checkConnectionClose(int response, int clientSocket){
    if(!response){
        close(clientSocket);
        return 1;
    }
    return 0;
}
